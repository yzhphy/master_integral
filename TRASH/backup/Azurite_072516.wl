(* ::Package:: *)

(* ::Title:: *)
(*Azurite*)


(* ::Section:: *)
(*Head*)


HighestPower::usage="An option to for the highest power of IBPs in the interm: default 4";
WorkingPower::usage="An option to for the highest power of reduced IBPs: default 4";
DebugMode::usage="Debug mode for VertexBacktrack";


NumericMode::usage="An option on if the numeric computation is used: default true";
Characteristic::usage="An option for computation over a finite field: it is valid only in numeric computation, and the default value is 0 (rational numbers)";
LinearReductionMethod::usage="An option for the linear reduction method of IBPs";
NumericD::usage="An option on the numeric value of D which should be a positive rational number. If the value is non-positve, then analytic dimension will be used";
SyzygyMonomialOrder::usage="An option for the syzygy algorithm";
IBPOutput::usage="An option to determine if the IBP list is given in the output";
WatchingMode::usage="An option to determine if the intermediate steps are to be watched";
Symmetry::usage="An option to determine how symmetries are implemented in the integral reduction";
SymmetryPowerLimit::usage="Highest symmetry relations used for the system";


(* ::Section:: *)
(*Preparation*)


(* ::Subsection:: *)
(*Baikov parameterization preparation *)


Preparation[]:=Module[{InvGram,l,xs,preSolution,NontrivialSymmetries,dependentMomentum},
Clear[Global`L,Global`n,Global`Dim,Global`Gram,Global`x,Global`\[Mu],Global`vNV,Global`vNVPropagators,Global`vNVvar,
Global`var,Global`BaikovRep,Global`ext,Global`flow,\[Lambda]];

	Global`L=Length[Global`LoopMomenta];   (*   number of loops *)


	dependentMomentum=Complement[Range[Length[ExternalMomenta]+1],ToExpression[StringReplace[ToString[#],LetterCharacter->""]]&/@Global`ExternalMomenta];

	If[Length[dependentMomentum]==1,Global`ext=Insert[ExternalMomenta,-Total[ExternalMomenta],dependentMomentum],
							Global`ext=Append[ExternalMomenta,-Total[ExternalMomenta]];
	];
	Global`n=Length[Global`ext];       (*   number of external particles *)


	Global`dualExternalMomenta=ToExpression["p"<>ToString[#]]&/@Global`ExternalMomenta;
	Global`dualLoopMomenta=ToExpression["p"<>ToString[#]]&/@Global`LoopMomenta;

	Global`Momenta=Join[Global`ExternalMomenta,Global`LoopMomenta];
	Global`Dim=L(n-1)+1/2 L(L+1);      (*   Dimension of the polynomial ring, not the spacetime dimension *)
	If[Dim!=Length[Global`Propagators],Print["Wrong dimension... "]; Return[];];


	Global`Gram=Table[ExternalMomenta[[i]]ExternalMomenta[[j]],{i,1,n-1},{j,1,n-1}]/.Kinematics;
	Global`flow=Transpose[FactorList[#]][[1]]&/@SeriesCoefficient[Global`Propagators/.((#->\[Lambda] #)&/@Global`Momenta),{\[Lambda],0,2}];
	Global`flow=Table[Select[Global`flow[[i]],Intersection[Variables[#],Global`Momenta]!={}&][[1]],{i,1,Length[Global`flow]}];    (* The "square root" of propagators *)

	Global`massList=Join[(Expand[#^2]/.Global`Kinematics&)/@Global`ext,Global`Propagators/.(#->0&/@Join[Global`Momenta,Global`ExternalMomenta])];

	InvGram=Inverse[Gram];
	xs=Table[x[i,j],{i,1,Global`L},{j,1,Global`n-1}];



	Global`SP=Join[Flatten[Table[y[i,j],{i,1,L},{j,i,L}]],Flatten[xs]];   (* Scalar products *)
	Global`ScalarProduceRep=Join[Kinematics,Table[Global`LoopMomenta[[i]] Global`LoopMomenta[[j]]->y[i,j],{i,1,L},{j,i,L}],
					Table[Global`LoopMomenta[[i]] Global`ExternalMomenta[[j]]->x[i,j],{i,1,Global`L},{j,1,Global`n-1}]]//Flatten;
	Global`SPpropagators=Expand[Global`Propagators]/.Global`ScalarProduceRep;


	(* van Neerven-Vermaseren basis *)

	Global`vNV=Join[Kinematics,Table[Global`LoopMomenta[[i]] Global`ExternalMomenta[[j]]->x[i,j],
	{i,1,Global`L},{j,1,Global`n-1}]//Flatten,Table[Global`LoopMomenta[[i]] Global`LoopMomenta[[j]]->xs[[i]].InvGram.xs[[j]]-\[Mu][i,j],
	{i,1,Global`L},{j,i,Global`L}]//Flatten]//Factor;
	Global`vNVPropagators=(Total[MonomialList[#,Global`Momenta]/.vNV]&/@Global`Propagators);

	Global`vNVvar=Join[xs,Table[\[Mu][i,j],{i,1,L},{j,i,L}]]//Flatten;
	Global`var=Table[z[j],{j,1,Global`Dim}];
	Global`trivialization=#->1&/@Global`var;


(* Global`BaikovRep=Solve[Global`vNVPropagators==Global`var,Global`vNVvar][[1]]//Factor; *)

	preSolution=SingularSlimgb[Global`vNVPropagators-Global`var,Global`vNVvar,MonomialOrder->DegreeReverseLexicographic];
	Global`BaikovRep=Solve[preSolution==0,Global`vNVvar][[1]];

	Global`kernel=Det[Table[\[Mu]@@Sort[{i,j}],{i,1,L},{j,1,L}]];
	Global`ker=Global`kernel/.Global`BaikovRep;


	Global`Parameters=Join[Complement[Variables[Global`ker],Global`var],{d}];
	If[n>4,Global`kernelPower=(d-5-Global`L)/2,Global`kernelPower=(d-Global`L-Global`n)/2];    (* This gives the exponent of the Baikov polynomial *)

];


(* Rewrite a scalar product as the linear combination of propagators *)
PropagatorsExpand[f1_]:=Module[{f=f1,cc,d,Ansatz,list,sol},
		Ansatz=(Expand[f]/.Global`ScalarProduceRep)-Sum[cc[j] Global`SPpropagators[[j]],{j,1,Global`Dim}]-d;
		list=#[[2]]&/@CoefficientRules[Ansatz,Global`SP];
		sol=Solve[list==0,Append[Table[cc[j] ,{j,1,Global`Dim}],d]];
		If[sol!={},
			Return[Sum[cc[j] z[j],{j,1,Global`Dim}]+d/.sol[[1]]],
			Return[Null]
		];

];


(* ::Section::Closed:: *)
(*Syzygy*)


(* This convert a syzygy to an IBP, if the cofactor IS included. *)

Syzygy2IBP[list_,dList_,var_,powerList_]:=-powerList.Take[list,-Length[dList]]+ Div[Take[list,Length[var]],var];


monomialAnsatz[var_,deg_]:=(Times@@MapThread[#1^#2&,{var,Exponent[#,var]}])&/@MonomialList[(1+Total[var])^deg//Expand,var,DegreeReverseLexicographic];
monomialDegreeList[var_,degree_,morder_]:=#/(#/.Table[var[[j]]->1,{j,1,Length[var]}])&/@MonomialList[Total[var]^degree,var,morder];

std[f1_,ISP_]:=Module[{f=f1,list,Trivialization,shift},
	If[f==0,Return[0]];
	list=CoefficientRules[f,Global`var]//Factor;
	shift=Exponent[(Times@@Global`var)/.(#->1&/@ISP),Global`var];
	Return[Total[#[[2]]*(Int@@(shift-#[[1]]))&/@list]];
];



FactorPickup[f_,var_]:=Times@@((#[[1]]^#[[2]])&/@(Select[FactorList[f],Intersection[Variables[#],var]!={}&])); 


(* Only list the independent external momenta *)


LTdegree[f_,var_,numeric_]:=Exponent[First[MonomialList[f/.numeric,var,DegreeReverseLexicographic]],var]//Total;


(* PowerCounting[monomial1_]:=Module[{monomial=monomial1,list,i},
	list=Exponent[monomial,Global`var];
	For[i=1,i<=7,i++,If[list[[i]]>0,list[[i]]--]];
	Return[Total[list]];
]; *)

PowerCounting[monomial1_,ISPIndex1_]:=Module[{monomial=monomial1,list,i,ISPIndex=ISPIndex1},
	list=Exponent[monomial,Global`var];
	For[i=1,i<=Global`Dim,i++,If[list[[i]]>0&&!MemberQ[ISPIndex,i],list[[i]]--]];
	Return[Total[list]];
]; 

Integral2monomial[f_,ISP_]:=((Times@@Global`var)/.(#->1&/@ISP)) Times@@((Global`var)^-(f/.Int->List)); 



(* ::Subsection:: *)
(*Linear algebra tools*)


IndependentRow[M1_,characteristic_]:=Module[{M=M1,MTReduceT,i,ip,n,result,p=characteristic},	
	MTReduceT=Transpose[RowReduce[Transpose[M],Modulus->p]];
	result={};
	ip=1;
	n=Length[MTReduceT[[1]]];

	For[i=1,i<=Length[MTReduceT],i++,
		If[ip>n,Break[]];
		If[MTReduceT[[i]]==SparseArray[ip->1,n],AppendTo[result,i]; ip++];
	];
	Return[result];
];

RowOperation[M1_]:=Module[{matrix=M1,m,n,rmatrix,a,result},
		m=Length[matrix];
		n=Length[matrix//Transpose];
		
		rmatrix=RowReduce[Transpose[Join[Transpose[matrix],IdentityMatrix[m]]]];

		result=(Transpose[rmatrix][[1;;n]])//Transpose;

		a=(Transpose[rmatrix][[n+1;;n+m]])//Transpose;
		Return[{result,a}];
];


(* To find pivots in a row reduced matrix *) 
Pivots[Matrix_]:=Module[{M=Matrix,m,n,i,rowpointer,columnpointer,result={}},
	m=Length[M];
	
	n=Length[M[[1]]];
	rowpointer=1;

	For[columnpointer=1,columnpointer<=n,columnpointer++,
		If[M[[rowpointer,columnpointer]]==0,Continue[]];

		AppendTo[result,columnpointer];
		rowpointer++;
		If[rowpointer>m,Break[]];
	];
	Return[result];
];


RecursiveReduce[fList1_,var1_,startupSteps1_,numeric1_,characteristic_]:=Module[{fList=fList1,var=var1,startupSteps=startupSteps1,numeric=numeric1,newEqns,
redList,ttt,i,n,Matrix,TargetList,indiceSet,p=characteristic},

		ttt=AbsoluteTime[];
		n=Length[fList];

		If[n<=startupSteps,Return[SingularSlimgb[fList//Flatten,var,Modulus->p,MonomialOrder->DegreeReverseLexicographic]]];
		TargetList=Flatten[fList[[Range[startupSteps]]]];		
		redList=SingularSlimgb[TargetList,var,Modulus->p,MonomialOrder->DegreeReverseLexicographic];
	
		Print["reduction step ",startupSteps," finished... ",AbsoluteTime[]-ttt," ",Length[redList]," Relations"];
		ttt=AbsoluteTime[];
		For[i=startupSteps+1,i<=n,i++,
		
			TargetList=Join[redList,fList[[i]]];
		
			Matrix=Coefficient[#,var]&/@(TargetList)/.numeric;
			
			indiceSet=IndependentRow[Matrix,p];
		
			(* If[i<n,redList=SingularSlimgb[TargetList[[indiceSet]],var,MonomialOrder->DegreeReverseLexicographic],
				redList=SingularStd[TargetList[[indiceSet]],var,MonomialOrder->DegreeReverseLexicographic]]; *)

			redList=SingularSlimgb[TargetList[[indiceSet]],var,Modulus->p,MonomialOrder->DegreeReverseLexicographic];
		
	
			Print["reduction step ",i," finished... ",AbsoluteTime[]-ttt," ",Length[redList]," Relations"]; 
			
			ttt=AbsoluteTime[];
		];

		Return[redList];
];


(* ::Section:: *)
(*Graph theory and symmetry*)


GraphPrepare[propagatorIndex1_]:=Module[{propagatorIndex=propagatorIndex1,props},

	props=Global`flow[[propagatorIndex]];
	Global`Nvertex=1+Length[props]-L; 

	Global`edges=Join[Global`ext,props];
	Global`edgeMass=Join[massList[[;;Global`n]],massList[[propagatorIndex+Global`n]]];

	Global`edgeList=Join[Global`ext,props,props];
	Global`edgeListID=Join[Range[Length[ext]],Range[Length[ext]+1,Length[ext]+Length[props]],Range[Length[ext]+1,Length[ext]+Length[props]]];

	Global`vertexStack=Table[Null,{j,1,Global`Nvertex}];
	Global`connectionMatrix=Table[{},{i,1,Global`Nvertex+Global`n},{j,1,Global`Nvertex+Global`n}];
	Global`AdjMatrix=Table[0,{i,1,Global`Nvertex+Global`n},{j,1,Global`Nvertex+Global`n}];
];

connection[depth_]:=Module[{i,currentVertex,extLines,selfEdge},
currentVertex=Global`edgeListID[[vertexStack[[depth]]]];

For[i=1,i<=depth-1,i++,	
connectionMatrix[[i+Global`n,depth+Global`n]]=connectionMatrix[[depth+Global`n,i+Global`n]]=Intersection[Global`edgeListID[[vertexStack[[i]]]],currentVertex];
];

selfEdge=Union[currentVertex];
connectionMatrix[[depth+Global`n,depth+Global`n]]={};
For[i=1,i<=Length[selfEdge],i++,	If[Count[currentVertex,selfEdge[[i]]]==2,AppendTo[connectionMatrix[[depth+Global`n,depth+Global`n]],selfEdge[[i]]];
];
];

For[i=1,i<=Global`n,i++,
connectionMatrix[[i,depth+Global`n]]=connectionMatrix[[depth+Global`n,i]]={};
];   (* reset the external lines connecting to the current vertices *)

extLines=Select[currentVertex,#<=Global`n&];
For[i=1,i<=Length[extLines],i++,
connectionMatrix[[extLines[[i]],depth+Global`n]]=connectionMatrix[[depth+Global`n,extLines[[i]]]]={extLines[[i]]};
];
];


Options[VertexBacktrack]={DebugMode->False};
VertexBacktrack[indices1_,depth_,OptionsPattern[]]:=Module[{maxLength,i,j,Attempt,nn,subset,flag,graph,DebugFlag=OptionValue[DebugMode],indices=indices1,signFlag,signList},

If[depth==1,Global`TrackLog={};indices=Range[Length[edgeList]]];  (* Start with all indices *)

If[DebugFlag==True,Print[depth]];
AppendTo[Global`TrackLog,depth];
If[depth==Global`Nvertex,
	vertexStack[[depth]]=indices;
	connection[depth];
	Global`AdjMatrix=Map[Length,connectionMatrix,{2}];
	graph=AdjacencyGraph[Global`AdjMatrix,VertexLabels->"Name"];
	

	If[ConnectedGraphQ[graph]&&VertexCount[graph]==Global`Nvertex+Global`n,Return[graph];];

	Return[-1];
	
];

nn=Length[indices];
maxLength=nn-3(Global`Nvertex-depth);

If[maxLength<3,Return[-1]];
Attempt=Sum[Binomial[nn,j],{j,3,maxLength}];   (* To be improved *)

For[i=1,i<=Attempt,i++,
	subset=Subsets[indices,{3,maxLength},{i}][[1]];
	
	If[PolynomialMod[Total[Global`edgeList[[subset]]],2]==0,
		
		(* Basic sign check *)

		signFlag=0;

		For[j=0,j<=2^(Length[subset]-1)-1,j++,
			signList=(-1)^IntegerDigits[j,2,Length[subset]];
			If[signList.Global`edgeList[[subset]]==0//Expand,
				signFlag=1;
				Break[];];
		];
		If[signFlag==0,Continue[];];

		(* Additional checks here *) 

		If[SubsetQ[Range[Global`n],edgeListID[[subset]]],
			Continue[];
		];  (* no tree *) 
		If[Length[Union[edgeListID[[subset]]]]==Length[edgeListID[[subset]]]/2,
			Continue[];
		];    (* no isolated vertex *) 

		(* Additional checks end *) 
		
		If[DebugFlag==True,Print[depth," ",i,": ",Global`edgeList[[subset]]]];
		
		Global`vertexStack[[depth]]=subset;
		connection[depth];
		
		graph=VertexBacktrack[Complement[indices,subset],depth+1,DebugMode->DebugFlag];
		If[GraphQ[graph],Return[graph]];
	];
];
Return[-1]; 

];


FeynmanDiagram[propagatorIndex1_]:=Module[{propagatorIndex=propagatorIndex1,graph},
	GraphPrepare[propagatorIndex];
	graph=VertexBacktrack[{},1];
	Return[graph];
];


GraphTrim[graph_]:=AdjacencyGraph[Table[If[i==j,0,If[#[[i,j]]>0,1,0]],{i,1,Length[#]},{j,1,Length[#]}]]&[Normal[AdjacencyMatrix[graph]]];


weightedGraphAutomorphismGroup[graph_,weightmatrix_]:=Module[{GElements,M,i,group={}},
		If[!SimpleGraphQ[graph],Return[Null];];

		GElements=GroupElements[GraphAutomorphismGroup[graph]];

		For[i=1,i<=Length[GElements],i++,
			M=Permute[Transpose[Permute[weightmatrix,GElements[[i]]]],GElements[[i]]];
			If[SameQ[M,weightmatrix],AppendTo[group,GElements[[i]]]];
		];
		Return[PermutationGroup[group]];
];


EdgeSymmetryGroup[graph1_]:=Module[{graph=graph1,G,GElements,i,M,Mdiff, 
EdgeGroup={},simpleGraphFlag,multiLegs},
		
		simpleGraphFlag=SimpleGraphQ[graph];
		If[simpleGraphFlag,G=GraphAutomorphismGroup[graph],G=weightedGraphAutomorphismGroup[graph//GraphTrim,Global`AdjMatrix]];



		GElements=GroupElements[G];

		For[i=1,i<=Length[GElements],i++,
				
				M=Permute[Transpose[Permute[connectionMatrix,GElements[[i]]]],GElements[[i]]];
				Mdiff=Table[MapThread[#1->#2&,{connectionMatrix[[i,j]],M[[i,j]]}],{i,1,Length[M]},{j,i+1,Length[M]}];
				AppendTo[EdgeGroup,FindPermutation[Range[Length[edges]],Range[Length[edges]]/.Flatten[Mdiff]]];
		];

		If[!simpleGraphFlag,
			multiLegs=Union[Select[Flatten[connectionMatrix,1],Length[#]>1&]];  (* select the multiple edges *)
			EdgeGroup=Union[EdgeGroup,Flatten[{Cycles[{{#[[1]],#[[2]]}}],Cycles[{#}]}&/@multiLegs]];
		];

		Return[PermutationGroup[EdgeGroup]];
];


EdgeTransformRead[rule_]:=Module[{newEdges,extSolution,dual,ResEqn,M,Mextened,vectors,a,B,Eqn,sol,newMomenta,newGram},
		
		If[rule==Cycles[{}],Return[MapThread[#1->#2&,{Global`Momenta,Global`Momenta}]]];     (* Trivial transformation *)

		If[!SameQ[Global`edgeMass,Permute[Global`edgeMass,rule]],Return[Null]];    (*  Transformation violates the mass condition *)
		dual=MapThread[#1->#2&,{Join[Global`ExternalMomenta,Global`LoopMomenta],Join[Global`dualExternalMomenta,Global`dualLoopMomenta]}];

		newEdges=(Permute[Global`edges,rule]/.dual);
	
		extSolution=Solve[newEdges[[1;;Global`n]]-Global`ext==0,dualExternalMomenta];
		If[extSolution=={},Return[Null]];   (* Determine the permutation of external legs *)
	
		newGram=Table[Expand[(Global`dualExternalMomenta[[i]] Global`dualExternalMomenta[[j]])/.extSolution[[1]]]/.Global`Kinematics,{i,1,Global`n-1},{j,1,Global`n-1}];
		newGram=newGram//Simplify;

	
		If[!SameQ[newGram,Global`Gram],Return[]];   (* Drop the symmetry which changes the kinematics *)
	
		ResEqn=Table[(newEdges[[j]]-c[j] Global`edges[[j]])/.extSolution[[1]],{j,Global`n+1,Length[Global`edges]}];
		

		vectors=Join[Global`dualLoopMomenta,Global`ExternalMomenta,Global`LoopMomenta];
		Mextened=Coefficient[#,vectors]&/@ResEqn;
		M=Coefficient[#,Global`dualLoopMomenta]&/@ResEqn; 

		If[MatrixRank[M]!=Global`L,Return[Null]];
		a=RowOperation[M][[2]];
		B=a.Mextened;

		Eqn=Flatten[B[[Global`L+1;;]]];
		sol=Solve[Eqn==0,Variables[Eqn]];

		
		If[Length[sol]==0,Return[Null]];

		newMomenta=Join[Global`dualExternalMomenta,Global`dualLoopMomenta]/.extSolution[[1]]/.(Solve[(B[[;;Global`L]].vectors==0)/.sol[[1]],Global`dualLoopMomenta][[1]]);

		
		Return[MapThread[#1->#2&,{Global`Momenta,newMomenta}]];

];


 SymmetryDetection[indices_]:=Module[{graph,symmetries={},GE,i,replacement,ISP,ISPindices,cut,newISP,ISPz},
	graph=FeynmanDiagram[indices];
	If[!Graph[graph],Return[symmetries]];
	GE=GroupElements[EdgeSymmetryGroup[graph]];    (* To get the symmetry group of legs *)
	ISPindices=Complement[Range[Global`Dim],indices];
	ISPz=z/@ISPindices;
	ISP=Global`Propagators[[ISPindices]];
	cut=z[#]->0&/@indices;

	For[i=1,i<=Length[GE],i++,
		replacement=EdgeTransformRead[GE[[i]]];

		If[replacement==Null,Continue[]];
		
		newISP=Factor[PropagatorsExpand[#/.replacement]/.cut]&/@ISP;
		AppendTo[symmetries,MapThread[#1->#2&,{ISPz,newISP}]];
		
	];
		Return[Union[symmetries]];
]; 


CheckSymmetry[F_,Trans_,Numeric_,p_]:=If[Expand[(F/.Trans/.Numeric)-(F/.Numeric),Modulus->p]===0,True,False];


(* ::Section:: *)
(*Integral reduction*)


Options[IntegralRed]={HighestPower->4,MonomialOrder->DegreeReverseLexicographic,SyzygyMonomialOrder->DegreeReverseLexicographic,NumericMode->False,Characteristic->0,
LinearReductionMethod->"Singular",NumericD->Null,IBPOutput-> False,WorkingPower->4, WatchingMode->False,Symmetry->"None",SymmetryPowerLimit->1};


IntegralRed[propagators_,OptionsPattern[]]:=Module[{props=propagators,Cut,dListCut,k,n,num,powerList,syz,m,ttt=AbsoluteTime[],timer=AbsoluteTime[]
,IBP,IBPgen,IBPfactor,degree,localPowerLimit,seeds,i,j,IBPdegree,IntegralList,
ReducedIBPs,PowerVariables,IBPlist,Totaldeg,LinearReduceMethod=OptionValue[LinearReductionMethod],IBPsorted,deg,
Numericflag=OptionValue[NumericMode],IBPGen,IBP1,IBPMatrix,syzCounting,dimensionReplacement,
InternalNumeric,IBPs,indiceSet,degreeSet,MinimalIBPsort,ISPIndex,ISP,F,TransformationCut
,varCut,monomialList,HighestDegree=OptionValue[HighestPower],Morder=OptionValue[MonomialOrder],syzMorder=OptionValue[SyzygyMonomialOrder],
p=OptionValue[Characteristic],ND=OptionValue[NumericD],IBPFlag=OptionValue[IBPOutput],WorkingDegree=OptionValue[WorkingPower]
,singularIdeal,MI,Trans,abandonedIntegrals,WatchingFlag=OptionValue[WatchingMode],M,SymmetryFlag=OptionValue[Symmetry],
symmetryDegree=OptionValue[SymmetryPowerLimit],transformation},

If[Numericflag==False,p==0];   (* Finite field only works for numeric computation *)
If[Numericflag==True&&NumberQ[ND],       
		dimensionReplacement={d->ND};
		InternalNumeric=Join[Global`numeric,{d->ND}];
		,
		dimensionReplacement={};
		InternalNumeric=Join[Global`numeric,{d->137/17}];
]; (* numeric dimension only works for numeric computation *)

If[WorkingDegree>HighestDegree,WorkingDegree=HighestDegree]; (* WorkingDegree is the degree limit for reduced IBPs, while HighestDegree is the degree limit for IBPs in the intermediate step *)

ISPIndex=Complement[Range[Dim],props];
ISP=z/@ISPIndex;

If[WatchingFlag==True,Print["ISP: ",ISP]];

Cut=z[#]->0&/@props;
varCut=Select[Global`var/.Cut,!IntegerQ[#]&];    (* free variables on the cut *) 



If[WatchingFlag==True,Print["Denominators: ",props]];
(* If[Intersection[ISPIndex,CutIndex]!={},Print["ISP cannot be on shell..."];Return[-1];]; *)
	(* num=Times@@NumList; *)


If[Numericflag==True,F=Global`ker/.Cut/.Global`numeric,F=Global`ker/.Cut];

F=FactorPickup[F,varCut];
Global`FF=F;

If[p>0,F=PolynomialMod[F,p]];



dListCut={F};
(* Print["Uncut denominators and the kernel: ",Join[dListShort,{FF}]]; *)

k=Length[dListCut];


(* numDegree=Total[Exponent[num,dListShort]]; *)

powerList={Global`kernelPower/.dimensionReplacement};
If[WatchingFlag==True,Print["Degrees ",powerList]];

(* -------------------------  Syzygy Generator   ------------------------- *)

ttt=AbsoluteTime[];

If[WatchingFlag==True,Print["Characteristic ",p]];

singularIdeal=Join[D[F,{varCut}],{F}];
If[p>0,singularIdeal=PolynomialMod[singularIdeal,p]];


			syz=Singular`SingularSyz[singularIdeal,varCut,MonomialOrder->syzMorder,Modulus->p];


If[WatchingFlag==True,Print["Syzyzgies are found ",AbsoluteTime[]-timer, " seconds"]];
If[WatchingFlag==True,Print[Length[syz]," Syzygies"]];

ttt=AbsoluteTime[];



(* -------------------------  IBP preparation   ------------------------- *)
IBP={};
IBPsorted=Table[{},{i,0,HighestDegree}];
syzCounting=Table[0,{j,1,Length[syz]}];

(* -------------------------  Symmetry   ------------------------- *)
TransformationCut={};
If[SymmetryFlag=="Custom",
	For[i=1,i<=Length[Global`Symmetries],i++,	
			transformation=MapThread[#1->#2&,{Global`var,Global`Symmetries[[i]]}]/.InternalNumeric;

			Print[transformation];
			Trans=Table[varCut[[j]]->(varCut[[j]]/.transformation/.Cut),{j,1,Length[varCut]}];
			If[CheckSymmetry[F,Trans,Global`numeric,p],AppendTo[TransformationCut,Trans]];
	];

	TransformationCut=Complement[TransformationCut,{MapThread[#1->#2&,{varCut,varCut}]}];
	Print["Symmetries for this cut: ",Length[TransformationCut]];

];
If[SymmetryFlag=="Automatic",
		
		TransformationCut=Select[SymmetryDetection[props]/.InternalNumeric,CheckSymmetry[F,#,InternalNumeric,p]&];
		TransformationCut=Complement[TransformationCut,{MapThread[#1->#2&,{varCut,varCut}]}];
		Print["Symmetries for this cut: ",Length[TransformationCut]];
];


If[Length[TransformationCut]>0,

	For[i=1,i<=Min[HighestDegree,symmetryDegree],i++,
				monomialList=monomialDegreeList[varCut,i,DegreeReverseLexicographic];
				IBPsorted[[i+1]]=Flatten[Table[std[#-(#/.TransformationCut[[j]]),ISP],{j,1,Length[TransformationCut]}]&/@monomialList];
	];
	If[WatchingFlag==True,Print["Symmetry relations: ",Length/@IBPsorted]];
];


For[i=1,i<=Length[syz],i++,
	
	IBPGen=Syzygy2IBP[syz[[i]],dListCut,varCut,powerList];   (*  Find the "simplest IBP" first  *)
	If[p>0,IBPGen=PolynomialMod[IBPGen,p]];

	IBPGen=Total[MonomialList[IBPGen,varCut]];
	IBPdegree=LTdegree[IBPGen,varCut,InternalNumeric]; 

	If[IBPdegree<0,IBPdegree=0];
	localPowerLimit=HighestDegree - IBPdegree;  (* Be careful here *)
	(* Print[localPowerLimit]; *)

	seeds=monomialAnsatz[varCut,localPowerLimit];

	For[j=1,j<=Length[seeds],j++,
		IBP1=seeds[[j]] IBPGen+Take[syz[[i]],Length[varCut]].D[seeds[[j]],{varCut}];
		If[p>0,IBP1=PolynomialMod[IBP1,p]];
		deg=LTdegree[IBP1,varCut,InternalNumeric];
		If[deg<0||deg>HighestDegree,Continue[]]; 
		(* If[deg>PowerLimit,Continue[]]; *)

		syzCounting[[i]]++;
	
    	AppendTo[IBPsorted[[deg+1]],std[IBP1,ISP]];;   (*************** Sort integrals by degree!  *************)
	];


];

If[WatchingFlag==True,
Print["IBPs found from each syzygy generator: ",syzCounting];
Print["IBPs are found... ",AbsoluteTime[]-ttt," seconds "];
Print["The number of IBPs generated: ",Length/@IBPsorted,"   ",Total[Length/@IBPsorted]];
];


IntegralList=Complement[IBPsorted//Variables,Global`Parameters];
If[WatchingFlag==True,Print["The number of integrals involved ",Length[IntegralList]]];



IntegralList=std[#,ISP]&/@MonomialList[Total[Integral2monomial[#,ISP]&/@IntegralList],Reverse[ISP],Morder];



(* IntegralList=Join[SortBy[Complement[IntegralList,MIList],-PowerCounting[Integral2monomial[#,ISP]]&],MIList]; *)

(* Print["The number of integrals involved, modified by the master integral ordering ",Length[IntegralList]]; *)



Global`IBPsort=IBPsorted;
Global`IntList=IntegralList;  



ttt=AbsoluteTime[];
If[WatchingFlag==True,Print[LinearReduceMethod]];
Switch[LinearReduceMethod,
	"Singular",
		IBPsorted=SortBy[#,ByteCount[#]&]&/@IBPsorted;

		ReducedIBPs=RecursiveReduce[IBPsorted,IntegralList,3,InternalNumeric,p];
		ReducedIBPs=ReducedIBPs//Reverse;    (* Row order convention in Singular *)
		If[WatchingFlag==True,Print["IBPs are simplified... ",AbsoluteTime[]-ttt]];


	,
	"SingularDirect",

		ReducedIBPs=SingularSlimgb[Flatten[IBPsorted],IntegralList,MonomialOrder->DegreeReverseLexicographic,Modulus->p];
		ReducedIBPs=ReducedIBPs//Reverse;    (* Row order convention in Singular *)


		If[WatchingFlag==True,Print["All IBPs are simplified... ",AbsoluteTime[]-ttt]];


	,
	"RowReduce",

		M=Coefficient[#,IntegralList]&/@Flatten[IBPsorted];

		M=RowReduce[M,Modulus->p];
		ReducedIBPs=M.IntegralList;
		If[WatchingFlag==True,Print["All IBPs are simplified... ",AbsoluteTime[]-ttt]];


];

If[WatchingFlag==True,Print["The number of independent IBPs ",Length[ReducedIBPs]]];


If[WorkingDegree<HighestDegree,
			abandonedIntegrals=Select[IntList,LTdegree[Integral2monomial[#,varCut],varCut,InternalNumeric]>WorkingDegree&];	
			ReducedIBPs=Select[ReducedIBPs,Intersection[Variables[#],abandonedIntegrals]=={}&];
			IntegralList=Select[IntegralList,!MemberQ[abandonedIntegrals,#]&];
];

{MI,IBPs}=IBPRead[ReducedIBPs,IntegralList,InternalNumeric,p];
Print["Total time used: ",AbsoluteTime[]-timer," seconds"];


If[IBPFlag==False,Return[MI],Return[{MI,IBPs}]];


];


IBPRead[reducedIBP1_,integralList1_,internalnumeric1_,characteristic_]:=Module[{reducedIBP=reducedIBP1,integralList=integralList1,internalnumeric=internalnumeric1,
p=characteristic,M,i,MIindex,MIs,IBPList},
			M=(Coefficient[#,integralList]&/@reducedIBP)/.internalnumeric;
			(* M=PolynomialMod[M,p]; *)   (* Is this step needed? *)

			

			MIindex=Complement[Range[Length[integralList]],Pivots[M]];
			MIs=integralList[[MIindex]];
			
			IBPList=Solve[reducedIBP==0,Complement[integralList,MIs]][[1]];
			Return[{MIs,IBPList}];
];
