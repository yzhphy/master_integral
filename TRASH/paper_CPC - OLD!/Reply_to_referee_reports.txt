Dear Editor,

We were happy to read the referees' largely positive review of our work.
We are moreover grateful for their careful reading of the manuscript and testing of our code.

In addition to the changes made to the manuscript in response 
to the referees' requests, please also note that we have updated 
the Acknowledgements section (adding "M. Kalmykov" to the list of people
acknowledged for useful discussions).


In response to the comments of referee #1:

1) Relations among edge-reducible integrals become trivial on their maximal cut
since the integrals, by nature of being edge-reducible, can be expressed as a 
linear combination of strict subdiagrams, all of which vanish on the maximal cut.
Therefore such relations do not provide additional information that could
reduce the basis further.

However, from the report of referee #2 we have learned that in rare cases 
the basis obtained by Azurite may be slightly overcomplete. This is because 
the cut-based approach misses IBP identities relating integrals belonging to 
different sectors. Moreover, identities relating integrals 
with different numbers of external legs are not detected.

We have therefore revised our manuscript as follows.

- Abstract: we have added as the last sentence:
  "In rare cases, the basis obtained by Azurite may be slightly overcomplete."

- Introduction, fourth-to-last paragraph. We have added as the last sentence:
  "In rare cases, the basis obtained by Azurite may be slightly overcomplete, 
  as will be discussed below."

- Section 2, second paragraph below bulleted list. We have added the following paragraph:
  "In rare cases, the basis obtained by {\sc Azurite} may be slightly overcomplete. 
  This is in part because the set of integral relations evaluated on some cut $C_1$ 
  does not necessarily exhaust all relations among integrals supported on $C_1$ and 
  integrals supported on some other cut $C_2$ where neither $C_1 \subseteq C_2$ nor 
  $C_2 \subseteq C_1$. Moreover, identities relating integrals with different kinematics 
  or mass configurations are not detected. Examples of the latter can be found in
  refs.~[Nucl.Phys. B 605 (2001) 266-318, Nucl.Phys.Proc.Suppl. 157 (2006) 58-62, Nucl.Phys. B 755 (2006) 221-238]."

- Conclusions, first paragraph. We have added as the second-to-last sentence:
  "In rare cases, the integral basis obtained by Azurite
  may be slightly overcomplete due to some integral relations being 
  missed by the cut-based approach."

2) IBP relations evaluated on their maximal cut provide sufficient information
to determine a basis of integrals. They do not, however, provide enough information
to reconstruct full IBP reductions. For this purpose, one may instead use
a spanning set of cuts as introduced in [Phys. Rev. D 93 (2016) no. 4, 041701].

We have added the following two sentences to the end of the last paragraph of Section 2.3:
"However, we note that IBP identities evaluated on maximal cuts 
do not provide enough information to reconstruct full off-shell IBP reductions. 
For this purpose, one may instead use a spanning set of cuts 
as introduced in [Phys. Rev. D 93 (2016) no. 4, 041701]."

We are currently working on a public code based on Azurite and the ideas of
[Phys. Rev. D 93 (2016) no. 4, 041701] to provide complete off-shell IBP reductions. This is in line
with the last sentence of the Conclusions:
"Furthermore, we are working on a public package to produce complete
IBP reductions efficiently, based on the present algorithm to find a
basis of integrals, and on the construction of IBP reductions on cuts
via syzygy computations [Phys. Rev. D 94 (2016) no. 11, 116015; 
Phys. Rev. D 93 (2016) no. 4, 041701]."

3) In the present work we focus on the case of single-power propagators 
and therefore prefer to stick with the compact <12...n> notation.

Our formalism does allow generalization to cases with higher-power propagators. 
Moreover, we are currently working on augmenting the existing code with this feature 
and plan to make the new code publically available in the near future.
To reflect this, we have added the following text to the manuscript.

- Introduction, added as second-to-last paragraph:
"In its present form, Azurite is restricted to treat only integrals with 
single-power propagators. However, the underlying formalism allows generalization 
to cases with higher-power propagators, and an augmentation of the code to 
treat higher-power propagators is planned to be added in the near future."

- Summary and Outlook, second paragraph: added after first sentence:
"One direction, currently being pursued, is to extend 
the existing code to enable treatment of integrals with higher-power propagators."

More specifically, the approach referred to in the Summary and Outlook 
is based on the Feynman parametrization of the involved integrals which allows 
for an efficient way to deal with higher-power propagators.


4) Introduction, p. 3, fourth paragraph: to motivate the fixed-power propagator approach
of our algorithm, we have added the following paragraph:
"This approach has the advantage of truncating the list of integrals that appear 
in the IBP identities, thereby simplifying the subsequent linear solving stage. 
With these IBP reductions in hand, the reductions that involve integrals with 
squared propagators can then be obtained in turn."


As for the minor comments, we have carried out the revisions requested with
the exception of those in points 7) and 9). Our changes/rebuttals are as follows.

1) We have replaced "shrinking propagators" by "pinching lines" on p. 3.

2) We have added a citation of ref. [48] above eq. (10).

3) P. 7, last paragraph: we have removed the superfluous "to".

4) Section 2.2, item 2 of bullet point list: we have corrected the reference
for diagram <145678> to "fig. 1b".

5) In eq. (B.6) we have added |_{cut} to the right of the integral on the left-hand side
to indicate that the integral under consideration is evaluated on the cut.

6) The missing "+..." in eq. (B.7) has been added.

7) The only occurence of \cal{I} is in eq. (2) where it denotes a vector of integrals
(as explained immediately above eq. (2)) rather than a single integral.
Hence there is no notational inconsistency.

8) The term "Mathematica" now appears uniformly throughout the text as {\sc Mathematica}.

9) In section 2.2, item 1 of the bulleted list, it is explained
that scaleless integrals are discarded in Azurite because they vanish in dimensional regularization.

10) We have added the sentence "We refer to section 2.3
for an explanation of the reasons behind these choices." as the second-to-last sentence
in the first paragraph below the bullet point list in section 2.



In response to the comments of referee #2:

Concerning usage across different platforms and versioning errors, 
we have corrected three issues. The first issue concerns the installation 
folder of Azurite. In the manual, the user is now asked to explicitly 
provide an installation directory AzuriteDirectory and then call
Get[AzuriteDirectory<>"Azurite.wl"];
to load Azurite.
The second issue was caused by the Singular/Mathematica interface 
not properly handling whitespace characters in folder names. 
The third issue was caused by some default settings of the 
functions having changed since the manual was written.
These include for example the setting "IBPOutput -> True" 
in IntegralRed (whereby the on-shell IBP reductions are provided along 
with the list of basis integrals). As a result, the output generated 
with the current version of Azurite would be different from that stored 
in the manual. We have updated the manual to be up-to-date with the new 
code and encourage the referee to compare the output stored in the updated 
manual with that generated by the new code, both available at
https://bitbucket.org/yzhphy/azurite/raw/master/release/Azurite_1.1.1.tar.gz
With these updates, we find that Azurite runs without problems on 
Ubuntu 14.04 and 16.04, Debian 8.6 and 9.0, and Mac-OS 10.12.

Concerning the labeling of edges, the present approach 
is guaranteed to work correctly for any multi-edged graph and has 
moreover been tested and shown to work in many examples. We agree with the 
referee that a correct labeling of edges can also be obtained by 
adding a vertex to each edge. However, as a result of the added vertices, 
the graphs thus rendered tend to have edges drawn with kinks rather than 
smooth edges. For aesthetic reasons we therefore prefer to stick to the existing code.
In terms of ease of usage, to display a multi-edge graph with labeled edges, 
the user simply has to call, for example,
FeynmanGraph[{1, 2, 3}, DiagramExtendedOutput -> True]. In our view, 
this code is therefore already user-friendly in its current form.

We agree with the referee that it is an implicit assumption of 
Azurite that the basis integrals can be determined on a sector-by-sector basis.
Cross-sector IBP relations are therefore missed by 
the underlying algorithm, as is moreover the relation in figure 3 of 
Nucl. Phys. Proc. Suppl. 157 (2006) 58-62. A possible remedy would be 
to avoid the use of cuts and determine and solve the integral identities 
numerically, but this is in practise much slower than the current algorithm.

We have therefore revised our manuscript as follows.

- Abstract: we have added as the last sentence:
  "In rare cases, the basis obtained by Azurite may be slightly overcomplete."

- Introduction, third-to-last paragraph. We have added as the last sentence:
  "In rare cases, the basis obtained by Azurite may be slightly overcomplete, 
  as will be discussed below."

- Section 2, second paragraph below bulleted list. We have added the following paragraph:
  "In rare cases, the basis obtained by {\sc Azurite} may be slightly overcomplete. 
  This is in part because the set of integral relations evaluated on some cut $C_1$ 
  does not necessarily exhaust all relations among integrals supported on $C_1$ and 
  integrals supported on some other cut $C_2$ where neither $C_1 \subseteq C_2$ nor 
  $C_2 \subseteq C_1$. Moreover, identities relating integrals with different kinematics 
  or mass configurations are not detected. Examples of the latter can be found in
  refs.~[Nucl.Phys. B 605 (2001) 266-318, Nucl.Phys.Proc.Suppl. 157 (2006) 58-62, Nucl.Phys. B 755 (2006) 221-238]."

- Conclusions, first paragraph. We have added as the second-to-last sentence:
  "In rare cases, the integral basis obtained by Azurite
  may be slightly overcomplete due to some integral relations being 
  missed by the cut-based approach."


Concerning replacements such as  m^2->1 in Numerics, we have now updated 
the code to make such replacements possible. We thank the referee for the 
suggestion and encourage him/her to download and test the latest version of Azurite available at
https://bitbucket.org/yzhphy/azurite/raw/master/release/Azurite_1.1.1.tar.gz


Sincerely,
Alessandro Georgoudis, Kasper J. Larsen and Yang Zhang