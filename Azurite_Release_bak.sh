rm -r Azurite_$1
mkdir Azurite_$1
mkdir Azurite_$1/code
mkdir Azurite_$1/example
mkdir Azurite_$1/manual
cp code/Azurite.wl Azurite_$1/code
cp example/*.nb Azurite_$1/example
cp manual/*.nb Azurite_$1/manual
tar -zcvf release/Azurite_$1.tar.gz Azurite_$1
