\contentsline {section}{\numberline {1}Introduction}{1}{section.1}
\contentsline {section}{\numberline {2}Algorithm}{3}{section.2}
\contentsline {subsection}{\numberline {2.1}Associated graphs and their symmetries}{5}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Adaptive parameterization and further graph simplifications}{7}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}IBP identities on maximal cuts and master integrals}{9}{subsection.2.3}
\contentsline {subsection}{\numberline {2.4}Geometric interpretation of syzygy equation}{11}{subsection.2.4}
\contentsline {section}{\numberline {3}Examples and performance}{12}{section.3}
\contentsline {subsection}{\numberline {3.1}Analytic IBPs at maximal cut}{14}{subsection.3.1}
\contentsline {section}{\numberline {4}Summary and Outlook}{16}{section.4}
\contentsline {section}{\numberline {A}Usage of Azurite}{17}{appendix.A}
\contentsline {subsection}{\numberline {A.1}Installation}{17}{subsection.A.1}
\contentsline {subsection}{\numberline {A.2}Commands and Options}{17}{subsection.A.2}
\contentsline {subsubsection}{\numberline {A.2.1}Path setup}{17}{subsubsection.A.2.1}
\contentsline {subsubsection}{\numberline {A.2.2}Kinematics and loop structure information}{17}{subsubsection.A.2.2}
\contentsline {subsubsection}{\numberline {A.2.3}Associated graphs and their discrete symmetries}{18}{subsubsection.A.2.3}
\contentsline {subsubsection}{\numberline {A.2.4}Master integrals}{19}{subsubsection.A.2.4}
\contentsline {subsubsection}{\numberline {A.2.5}Analytic IBP identities evaluated on their maximal cut}{22}{subsubsection.A.2.5}
