;; RefTeX parse info file
;; File: /Users/Beryllium/Dropbox/projects/Azurite_Development/paper_CPC/Azurite.tex
;; User: Beryllium (Yang Zhang)

(set reftex-docstruct-symbol '(


(xr nil "\\\\\\\\\\\\")

(index-tags)

(is-multi nil)

(bibview-cache ("Larsen:2015ped" . "Larsen & Zhang 2016, {Int. parts reduct. from unit., Phys. Rev. D93:041701"))

(master-dir . "/Users/Beryllium/Dropbox/projects/Azurite_Development/paper_CPC/")

(label-numbers)

(bof "/Users/Beryllium/Dropbox/projects/Azurite_Development/paper_CPC/Azurite.tex")

(toc "toc" "    1 Introduction" "/Users/Beryllium/Dropbox/projects/Azurite_Development/paper_CPC/Azurite.tex" nil 2 "1" "\\section{Introduction}" 9197)

("IBP" "e" "\\int \\prod_{j=1}^L \\bigg (\\frac{\\d^D l_j}{i \\pi^{D/2}}\\bigg) \\sum_{i=1}^L \\frac{\\partial }{\\partial " "/Users/Beryllium/Dropbox/projects/Azurite_Development/paper_CPC/Azurite.tex" nil)

("DE" "e" "\\frac{\\partial }{\\partial x_m} \\mathcal I({\\mathbf x},\\epsilon) =A_m({\\mathbf x},\\epsilon) \\mathcal " "/Users/Beryllium/Dropbox/projects/Azurite_Development/paper_CPC/Azurite.tex" nil)

(toc "toc" "    2 Algorithm" "/Users/Beryllium/Dropbox/projects/Azurite_Development/paper_CPC/Azurite.tex" nil 2 "2" "\\section{Algorithm}" 15318)

("Feynman" "e" "I[a_1,\\ldots, a_k;N]\\equiv \\int \\prod_{j=1}^L \\bigg (\\frac{\\d^D l_j}{i \\pi^{D/2}}\\bigg) \\frac{N(l_1," "/Users/Beryllium/Dropbox/projects/Azurite_Development/paper_CPC/Azurite.tex" nil)

("eq:2" "e" "\\la s_1 \\ldots s_m\\ra[N] \\equiv \\int \\prod_{j=1}^L \\bigg (\\frac{\\d^D l_j}{i \\pi^{D/2}}\\bigg) \\frac{N" "/Users/Beryllium/Dropbox/projects/Azurite_Development/paper_CPC/Azurite.tex" nil)

("invProp" "e" "D_i= \\Big(\\sum_{j=1}^L \\alpha_{ij} l_j +\\sum_{h=1}^n \\beta_{ih} k_h \\Big)^2 - m_i^2 \\hspace{1mm} \\eq" "/Users/Beryllium/Dropbox/projects/Azurite_Development/paper_CPC/Azurite.tex" nil)

("SP_counting" "e" "n_\\text{SP}=\\phi(n) L +\\frac{L(L+1)}{2} \\,, " "/Users/Beryllium/Dropbox/projects/Azurite_Development/paper_CPC/Azurite.tex" nil)

("eq:7" "e" "\\phi(n)\\equiv \\left\\{ \\begin{array}{cc} 4 & \\hspace{5mm} n\\geq 5 \\,," "/Users/Beryllium/Dropbox/projects/Azurite_Development/paper_CPC/Azurite.tex" nil)

("Baikov" "e" "\\la 1 2 \\ldots k \\ra[N] \\hspace{1mm} \\propto \\hspace{1mm} \\int \\d z_1 \\cdots \\d z_{n_\\text{SP}} F(z_" "/Users/Beryllium/Dropbox/projects/Azurite_Development/paper_CPC/Azurite.tex" nil)

(toc "toc" "      2.1 Associated graphs and their symmetries" "/Users/Beryllium/Dropbox/projects/Azurite_Development/paper_CPC/Azurite.tex" nil 3 "2.1" "\\subsection{Associated graphs and their symmetries}" 21665)

("graph_sym" "s" "Given the propagators in eq.~\\eqref{invProp}, it is useful to obtain the corresponding graph algorit" "/Users/Beryllium/Dropbox/projects/Azurite_Development/paper_CPC/Azurite.tex" nil)

("eq:3" "e" "M=\\{k_1,\\ldots, k_n, v_1,\\ldots, v_k, -v_1, \\ldots, -v_k\\} \\,. " "/Users/Beryllium/Dropbox/projects/Azurite_Development/paper_CPC/Azurite.tex" nil)

("eq:4" "e" "n_V=k-L+1 \\,, " "/Users/Beryllium/Dropbox/projects/Azurite_Development/paper_CPC/Azurite.tex" nil)

("pentabox_invProp" "e" "\\begin{alignedat}{4} & D_1 = l_1^2\\,, \\hspace*{0.27cm} && D_2 = (l_1 - k_1)^2\\,, \\hspace*{0.27cm} &&" "/Users/Beryllium/Dropbox/projects/Azurite_Development/paper_CPC/Azurite.tex" nil)

("eq:5" "e" "\\begin{alignedat}{2} & V_1=\\{k_1,-l_1,l_1-k_1\\}\\,, \\quad && V_2=\\{k_2,l_1 - K_{12},-l_1+k_1\\}\\,," "/Users/Beryllium/Dropbox/projects/Azurite_Development/paper_CPC/Azurite.tex" nil)

("pentagon-box" "f" "The pentagon-box diagram $\\la 12345678\\ra$." "/Users/Beryllium/Dropbox/projects/Azurite_Development/paper_CPC/Azurite.tex" nil)

("triangle-box" "f" "The $\\la 145678\\ra$ subdiagram of the pentagon-box diagram." "/Users/Beryllium/Dropbox/projects/Azurite_Development/paper_CPC/Azurite.tex" nil)

("fig:pentabox_family" "f" "Pentagon-box diagram and one of its subdiagrams." "/Users/Beryllium/Dropbox/projects/Azurite_Development/paper_CPC/Azurite.tex" nil)

("145678_G" "e" "D_1 \\mapsto D_4\\,, \\quad D_4 \\mapsto D_1 \\,,\\quad D_5 \\mapsto D_7\\,,\\quad D_7 \\mapsto D_5\\,, \\quad D" "/Users/Beryllium/Dropbox/projects/Azurite_Development/paper_CPC/Azurite.tex" nil)

("145678_Affine" "e" "k_4 \\mapsto k_5\\,, \\quad k_5 \\mapsto k_4\\,, \\quad l_1 \\mapsto -k_4-k_5-l_1\\,, \\quad l_2 \\mapsto -k_4" "/Users/Beryllium/Dropbox/projects/Azurite_Development/paper_CPC/Azurite.tex" nil)

("eq:6" "e" "v_i \\mapsto c_i v_{g(i)}\\,,\\quad \\forall i\\in\\{s_1,\\ldots, s_m\\} " "/Users/Beryllium/Dropbox/projects/Azurite_Development/paper_CPC/Azurite.tex" nil)

(toc "toc" "      2.2 Adaptive parameterization and further graph simplifications" "/Users/Beryllium/Dropbox/projects/Azurite_Development/paper_CPC/Azurite.tex" nil 3 "2.2" "\\subsection{Adaptive parameterization and further graph simplifications}" 28326)

("adp_red" "s" "To optimize the search for master integrals we apply the following simplifications during the study " "/Users/Beryllium/Dropbox/projects/Azurite_Development/paper_CPC/Azurite.tex" nil)

("tadpole" "f" "The subdiagram $\\la 12346\\ra$, which contains a massless tadpole." "/Users/Beryllium/Dropbox/projects/Azurite_Development/paper_CPC/Azurite.tex" nil)

("factorable" "f" "The subdiagram $\\la 1234567\\ra$, which is factorable." "/Users/Beryllium/Dropbox/projects/Azurite_Development/paper_CPC/Azurite.tex" nil)

("fig:pentabox_family" "f" "Some diagrams which can be simplified in the adaptive parametrization." "/Users/Beryllium/Dropbox/projects/Azurite_Development/paper_CPC/Azurite.tex" nil)

(toc "toc" "      2.3 IBP identities on maximal cuts and master integrals" "/Users/Beryllium/Dropbox/projects/Azurite_Development/paper_CPC/Azurite.tex" nil 3 "2.3" "\\subsection{IBP identities on maximal cuts and master integrals}" 32226)

("collapsible" "e" "\\la s_1 \\ldots s_m\\ra[N] \\hspace{1mm}=\\hspace{1mm} 0 + \\mbox{(strict subdiagrams)} \\,, " "/Users/Beryllium/Dropbox/projects/Azurite_Development/paper_CPC/Azurite.tex" nil)

("monomial_IBP" "e" "\\sum_i c_i \\la s_1 \\ldots s_m\\ra[N_i]= (\\cdots )\\,, " "/Users/Beryllium/Dropbox/projects/Azurite_Development/paper_CPC/Azurite.tex" nil)

("Baikov_mc" "e" "\\la s_1 \\ldots s_m\\ra[N] \\Big|_\\text{maximal cut} \\hspace{2mm} &\\propto \\hspace{2mm} \\int \\d z_{m+1}" "/Users/Beryllium/Dropbox/projects/Azurite_Development/paper_CPC/Azurite.tex" nil)

("IBP_Ansatz" "e" "&=\\int \\d z_{m+1} \\cdots \\d z_{n_\\text{SP}'} \\bigg(f^\\frac{D-h}{2}\\sum_{i=m+1}^{n_\\text{SP}'} \\frac{" "/Users/Beryllium/Dropbox/projects/Azurite_Development/paper_CPC/Azurite.tex" nil)

("syzygy" "e" "\\sum_{i=m+1}^{n_\\text{SP}'} a_i \\frac{\\partial f}{\\partial z_i} +a f=0 \\,, " "/Users/Beryllium/Dropbox/projects/Azurite_Development/paper_CPC/Azurite.tex" nil)

("IBP_maximal_cut" "e" "0= \\int \\d z_{m+1} \\cdots \\d z_{n_\\text{SP}'} f^\\frac{D-h}{2} \\bigg(\\sum_{i=m+1}^{n_\\text{SP}'} \\fra" "/Users/Beryllium/Dropbox/projects/Azurite_Development/paper_CPC/Azurite.tex" nil)

("eq:9" "e" "\\la s_1 \\ldots s_m\\ra \\bigg[\\sum_{i=m+1}^{n_\\text{SP}'} \\frac{\\partial a_i}{\\partial z_i} -\\frac{D-h" "/Users/Beryllium/Dropbox/projects/Azurite_Development/paper_CPC/Azurite.tex" nil)

("eq:10" "e" "\\mathbf g^{(j)}=(a_{m+1}^{(j)},\\ldots a_{n_\\text{SP}'}^{(j)},a^{(j)}), " "/Users/Beryllium/Dropbox/projects/Azurite_Development/paper_CPC/Azurite.tex" nil)

(toc "toc" "      2.4 Geometric interpretation of syzygy equation" "/Users/Beryllium/Dropbox/projects/Azurite_Development/paper_CPC/Azurite.tex" nil 3 "2.4" "\\subsection{Geometric interpretation of syzygy equation}" 41202)

("eq:8" "e" "\\sum_{i=m+1}^{n_\\text{SP}'} a_i \\frac{\\partial}{\\partial z_i} \\,, " "/Users/Beryllium/Dropbox/projects/Azurite_Development/paper_CPC/Azurite.tex" nil)

("eq:1" "e" "\\mbox{syz} \\bigg(\\frac{\\partial f}{\\partial z_{m+1}},\\ldots, \\frac{\\partial f}{\\partial z_{n_\\text{S" "/Users/Beryllium/Dropbox/projects/Azurite_Development/paper_CPC/Azurite.tex" nil)

("non-singular-hypersurface" "e" "I_s\\equiv \\bigg\\la \\frac{\\partial f}{\\partial z_{m+1}},\\ldots, \\frac{\\partial f}{\\partial z_{n_\\text" "/Users/Beryllium/Dropbox/projects/Azurite_Development/paper_CPC/Azurite.tex" nil)

(toc "toc" "    3 Examples and performance" "/Users/Beryllium/Dropbox/projects/Azurite_Development/paper_CPC/Azurite.tex" nil 2 "3" "\\section{Examples and performance}" 43520)

("pcsteup" "n" "The computations were carried out on a i7-6700, 32GB DDR4 RAM machine using \\Singular ~v4.0.3, with " "/Users/Beryllium/Dropbox/projects/Azurite_Development/paper_CPC/Azurite.tex" nil)

("tb_def" "f" "Azurite initialization for massless Triple-Box." "/Users/Beryllium/Dropbox/projects/Azurite_Development/paper_CPC/Azurite.tex" 45193)

("tb_def" "s" "] LoopMomenta = {l1, l2, l3}; ExternalMomenta = {k1, k2, k4}; Propagators = {l1^2, (l1 - k1)^2, (l1 " "/Users/Beryllium/Dropbox/projects/Azurite_Development/paper_CPC/Azurite.tex" nil)

("tribox_code" "s" "@*) Having declared the diagram, we can now proceed to compute the master integrals of the vector sp" "/Users/Beryllium/Dropbox/projects/Azurite_Development/paper_CPC/Azurite.tex" 45841)

("tb_almi" "f" "Example of use for \\emph{FindAllMIs}" "/Users/Beryllium/Dropbox/projects/Azurite_Development/paper_CPC/Azurite.tex" 46183)

("tripleboxMI" "s" "] MIs=FindAllMIs[{1,2,3,4,5,6,7,8,9,10},NumericMode -> True,NumericD -> 1119/37,Characteristic -> 90" "/Users/Beryllium/Dropbox/projects/Azurite_Development/paper_CPC/Azurite.tex" nil)

("tb_fg" "f" "Irreducible topologies for the massless triple-box diagram." "/Users/Beryllium/Dropbox/projects/Azurite_Development/paper_CPC/Azurite.tex" nil)

("mi3b" "e" "&\\langle 123456789\\hspace{0.8mm}10 \\rangle [ z_{11} , z_{13} , 1 ] \\quad \\langle 1236789\\hspace{0.8m" "/Users/Beryllium/Dropbox/projects/Azurite_Development/paper_CPC/Azurite.tex" nil)

("ispc" "e" "\\begin{alignedat}{3} &z_{4}=(l_3)^2\\,, \\quad &&z_{5}=(l_3+k_1+k_2)^2 \\,, \\quad &&z_{11}=(l_1+k_4)^2 " "/Users/Beryllium/Dropbox/projects/Azurite_Development/paper_CPC/Azurite.tex" nil)

("ex_rd" "f" "Computation time and number of master integrals for different topologies and mass configurations." "/Users/Beryllium/Dropbox/projects/Azurite_Development/paper_CPC/Azurite.tex" nil)

(toc "toc" "      3.1 IBP identities evaluated on their maximal cut" "/Users/Beryllium/Dropbox/projects/Azurite_Development/paper_CPC/Azurite.tex" nil 3 "3.1" "\\subsection{IBP identities evaluated on their maximal cut}" 50544)

(toc "toc" "    4 Summary and Outlook" "/Users/Beryllium/Dropbox/projects/Azurite_Development/paper_CPC/Azurite.tex" nil 2 "4" "\\section{Summary and Outlook}" 52937)

(toc "toc" "    * Acknowledgements" "/Users/Beryllium/Dropbox/projects/Azurite_Development/paper_CPC/Azurite.tex" nil 2 "*" "\\section*{Acknowledgements}" 54699)

(appendix . t)

(toc "toc" "    A Usage of Azurite" "/Users/Beryllium/Dropbox/projects/Azurite_Development/paper_CPC/Azurite.tex" nil 2 "A" "\\section{Usage of Azurite}" 55617)

("usage_azu" "s" "\\subsection{Installation} To install \\Azurite, it is necessary to install the computer algebra syste" "/Users/Beryllium/Dropbox/projects/Azurite_Development/paper_CPC/Azurite.tex" nil)

(toc "toc" "      A.1 Installation" "/Users/Beryllium/Dropbox/projects/Azurite_Development/paper_CPC/Azurite.tex" nil 3 "A.1" "\\subsection{Installation}" 55661)

(toc "toc" "      A.2 Commands and Options" "/Users/Beryllium/Dropbox/projects/Azurite_Development/paper_CPC/Azurite.tex" nil 3 "A.2" "\\subsection{Commands and Options}" 56808)

(toc "toc" "        A.2.1 Path setup" "/Users/Beryllium/Dropbox/projects/Azurite_Development/paper_CPC/Azurite.tex" nil 4 "A.2.1" "\\subsubsection{Path setup}" 56842)

(toc "toc" "        A.2.2 Kinematics and loop structure information" "/Users/Beryllium/Dropbox/projects/Azurite_Development/paper_CPC/Azurite.tex" nil 4 "A.2.2" "\\subsubsection{Kinematics and loop structure information}" 57394)

(toc "toc" "        A.2.3 Associated graphs and their discrete symmetries" "/Users/Beryllium/Dropbox/projects/Azurite_Development/paper_CPC/Azurite.tex" nil 4 "A.2.3" "\\subsubsection{Associated graphs and their discrete symmetries}" 59938)

(toc "toc" "        A.2.4 Master integrals" "/Users/Beryllium/Dropbox/projects/Azurite_Development/paper_CPC/Azurite.tex" nil 4 "A.2.4" "\\subsubsection{Master integrals}" 62614)

("appendix_mi" "s" "\\lsm{DiagramAnalysis[index]} provides the list of basis integrals for a given diagram, without consi" "/Users/Beryllium/Dropbox/projects/Azurite_Development/paper_CPC/Azurite.tex" nil)

(toc "toc" "        A.2.5 Analytic IBP identities evaluated on their maximal cut" "/Users/Beryllium/Dropbox/projects/Azurite_Development/paper_CPC/Azurite.tex" nil 4 "A.2.5" "\\subsubsection{Analytic IBP identities evaluated on their maximal cut}" 69097)

("integral_index" "e" "\\frac{D_{k+1}^{-a_{k+1}} \\cdots D_{n_\\text{SP}}^{-a_{n_\\text{SP}}}}{D_1^{a_1} \\cdots D_k^{a_k}}. " "/Users/Beryllium/Dropbox/projects/Azurite_Development/paper_CPC/Azurite.tex" nil)

(toc "toc" "    B Integrals with squared propagators in Baikov representation on maximal cuts" "/Users/Beryllium/Dropbox/projects/Azurite_Development/paper_CPC/Azurite.tex" nil 2 "B" "\\section{Integrals with squared propagators in Baikov representation on maximal cuts}" 70691)

("Baikov_sq_mc" "e" "&\\hspace{20mm} \\times N(0, \\ldots,z_j,\\ldots,0,z_{m+1},\\ldots , z_{n_\\text{SP}'}) \\,,  " "/Users/Beryllium/Dropbox/projects/Azurite_Development/paper_CPC/Azurite.tex" nil)

("pentabox_invProp" "e" "\\begin{alignedat}{4} & D_1 = l_1^2\\,, \\quad && D_2 = (l_1 - k_1)^2\\,, \\quad && D_3 = (l_1 - K_{12})^" "/Users/Beryllium/Dropbox/projects/Azurite_Development/paper_CPC/Azurite.tex" nil)

("eq:12" "e" "I[m_1,\\ldots m_9;D]=\\int\\frac{\\d^D l_1}{i \\pi^{D/2}}\\frac{\\d^D l_2}{i \\pi^{D/2}}\\frac{(l_1+k_4)^{-m_" "/Users/Beryllium/Dropbox/projects/Azurite_Development/paper_CPC/Azurite.tex" nil)

("eq:13" "e" "I[m_1,\\ldots m_9;D]=C(D) \\int \\prod_{i=1}^9 \\d z_i F(z)^{\\frac{D-6}{2}} \\frac{z_8^{-m_8}z_9^{-m_9}}{" "/Users/Beryllium/Dropbox/projects/Azurite_Development/paper_CPC/Azurite.tex" nil)

("sq" "e" "I[1,1,1,1,1,1,2,0,0;D]=\\frac{C(D)}{C(D-2)} ( a_1 B_1[D-2]+a_2 B_2[D-2])\\,,  " "/Users/Beryllium/Dropbox/projects/Azurite_Development/paper_CPC/Azurite.tex" nil)

("dim_shift" "e" "B_i[D]=\\frac{C(D)}{C(D-2)} \\big( T_{1i} B_1[D-2]+T_{2i} B_2[D-2]) + \\ldots\\,,  " "/Users/Beryllium/Dropbox/projects/Azurite_Development/paper_CPC/Azurite.tex" nil)

("eq:15" "e" "\\left( \\begin{array}{c} c_1" "/Users/Beryllium/Dropbox/projects/Azurite_Development/paper_CPC/Azurite.tex" nil)

("eq:11" "e" "c_1=-\\frac{(D-5)(3D-14)}{(D-6)t}\\,,\\quad c_2=-\\frac{2(D-5)(D-4)}{(D-6)st}\\,. " "/Users/Beryllium/Dropbox/projects/Azurite_Development/paper_CPC/Azurite.tex" nil)

("" "s" "%% References %% %% Following citation commands can be used in the body text: %% Usage of \\cite is a" "/Users/Beryllium/Dropbox/projects/Azurite_Development/paper_CPC/Azurite.tex" 76030)

(bib "/Users/Beryllium/Dropbox/projects/Azurite_Development/paper_CPC/azurite.bib")

(eof "/Users/Beryllium/Dropbox/projects/Azurite_Development/paper_CPC/Azurite.tex")
))

