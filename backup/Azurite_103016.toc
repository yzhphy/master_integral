\contentsline {section}{\numberline {1}Introduction}{1}{section.1}
\contentsline {section}{\numberline {2}Algorithm}{3}{section.2}
\contentsline {subsection}{\numberline {2.1}Graph and symmetry}{3}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Adaptive parameterization}{5}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}IBPs with maximal cut and master integrals}{7}{subsection.2.3}
\contentsline {section}{\numberline {3}Usage}{7}{section.3}
\contentsline {section}{\numberline {4}Examples and performances}{7}{section.4}
\contentsline {section}{\numberline {5}Summary and Outlook}{8}{section.5}
