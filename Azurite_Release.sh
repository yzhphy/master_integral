echo -n "Do you want to build Azurite "$1" (y/n)? "
read answer
if echo "$answer" | grep -iq "^y" ;then
    PathVar=~/Dropbox/projects/azurite
    tempVar=~/exchange
    rm $PathVar/release/Azurite_$1.tar.gz
    rm $PathVar/code/*.*
    rm $PathVar/example/*.*
    rm $PathVar/manual/*.*
    cp code/Azurite.wl $PathVar/code
    cp example/*.nb $PathVar/example
    cp manual/*.nb $PathVar/manual
    cd $PathVar
    tar -zcvf  $tempVar/Azurite_$1.tar.gz --exclude='*.DS_Store' code example manual
    mv  $tempVar/Azurite_$1.tar.gz $PathVar/release
else
    echo Stop
fi
